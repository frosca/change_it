package frosca.changeit.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import frosca.changeit.R;
import frosca.changeit.api.BaseApiService;
import frosca.changeit.api.responses.ExchageRatesResponse;
import frosca.changeit.application.App;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    App app;
    BaseApiService baseApiService;
    String TAG = "ABC";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        app = (App) getApplication();
        baseApiService = app.getBaseApiService();


        Call<ExchageRatesResponse> call = baseApiService.fetchExchangeRates(BaseApiService.defaultSha);

        call.enqueue(new Callback<ExchageRatesResponse>() {
            @Override
            public void onResponse(Call<ExchageRatesResponse> call, Response<ExchageRatesResponse> response) {
                if (response.isSuccessful()) {
                    // TODO: 01.10.2016 Implement logic here
                }
            }

            @Override
            public void onFailure(Call<ExchageRatesResponse> call, Throwable t) {
                // TODO: 01.10.2016 Implement logic here
            }
        });
    }
}