package frosca.changeit.api;

import frosca.changeit.api.responses.ExchageRatesResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface BaseApiService {
    String defaultSha = "0e8b6037f3444cda9feb8b575306938257e0f49df11194";

    @GET("/api/json/{SHA}")
    Call<ExchageRatesResponse> fetchExchangeRates(@Path("SHA") String sha);
}
