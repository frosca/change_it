package frosca.changeit.api.responses;

import java.util.List;

import frosca.changeit.model.BankWrapper;

public class ExchageRatesResponse {
    private String date;
    private String source;
    private List<BankWrapper> valcurs;

    public String getDate() {
        return date;
    }

    public String getSource() {
        return source;
    }

    public BankWrapper getValcurs() {
        return valcurs.get(0);
    }
}
