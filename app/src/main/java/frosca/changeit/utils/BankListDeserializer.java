package frosca.changeit.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import frosca.changeit.model.Bank;
import frosca.changeit.model.BankWrapper;

public class BankListDeserializer {
    public static Map<String, Bank> getBankMap(BankWrapper wrapper) {
        Map<String, Bank> map = new HashMap<String, Bank>();
        List<Bank> list = wrapper.getBankList();

        for (int i = 0; i < list.size(); i++) {
            Bank bank = list.get(i);
            map.put(bank.getNameShort(), bank);
        }
        return map;
    }
}
