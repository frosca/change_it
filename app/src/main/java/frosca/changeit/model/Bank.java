package frosca.changeit.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Bank {
    @SerializedName("ID")
    private String id;

    private String name;
    private String nameShort;
    private String type;
    private String updated;

    @SerializedName("valute")
    private List<Valuta> valuteList;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNameShort() {
        return nameShort;
    }

    public String getType() {
        return type;
    }

    public String getUpdated() {
        return updated;
    }

    public List<Valuta> getValuteList() {
        return valuteList;
    }
}