package frosca.changeit.model;

import com.google.gson.annotations.SerializedName;

public class Valuta {
    @SerializedName("char_code")
    private String code;

    @SerializedName("nominal")
    private int nominal;
    @SerializedName("sell")
    private double sell;
    @SerializedName("buy")
    private double buy;
    @SerializedName("value")
    private double value;

    public String getCode() {
        return code;
    }

    public int getNominal() {
        return nominal;
    }

    public double getSell() {
        return sell;
    }

    public double getBuy() {
        return buy;
    }

    public double getValue() {
        return value;
    }
}
