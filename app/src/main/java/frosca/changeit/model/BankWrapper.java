package frosca.changeit.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class BankWrapper {
    private String date;

    @SerializedName("banks")
    private List<Bank> bankList;

    public String getDate() {
        return date;
    }

    public List<Bank> getBankList() {
        return bankList;
    }
}